# Realtime_webservice

## Goal
It is responsible for controlling all Real-Time requests to different domains in SEON

## Requirements

| Program               | Version |
| --------------------- | ------: |
| RabbitMQ              | 3.8.16  |
| Python                | 3.8     |
| Pip                   | 21.1.1  |

You must configure Jira or Devops webhook to the server where you gonna run the realtime_webservice

for more information:
- [Jira webhook](https://developer.atlassian.com/server/jira/platform/webhooks/)
- [Devops webhook](https://docs.microsoft.com/pt-br/azure/devops/service-hooks/services/webhooks?view=azure-devops)

The link you may register will looks like this: `http://www.YOUR_DOMAIN/api/jira/sro/ORGANIZATION_UUID/CONFIGURATION_UUID`

## Usage:

Since it uses SRO_DB lib you must export the following environment variables:
* USERDB = Postgres's username
* PASSWORDDB = Postgres's password
* HOST = Postgres's host (usually localhost)
* DBNAME = Your database's name (must be already created) on Postgres

If you would like to use a Cloud AMQP you must export:
* CLOUDAMQP_URL = Url to connect (for more info https://www.cloudamqp.com/docs/index.html)

Clone this repo and follow the steps bellow:

```bash
python3 -m venv env && source env/bin/activate
```

```bash
pip install -r requirements.txt
```

```bash
./run.sh
```

