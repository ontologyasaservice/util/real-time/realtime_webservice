from app import app
import os
from skywalking import agent, config

config.init(collector_address=os.environ.get('SW_AGENT_COLLECTOR_BACKEND_SERVICES'), service_name='application_service')

agent.start()
   

if __name__=="main":
    
    debug = os.environ.get('DEBUG', True)
    if debug == True:
        app.run(debug=True)
    else:
        app.run(debug=False)