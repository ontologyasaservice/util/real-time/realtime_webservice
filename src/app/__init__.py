# Import flask and template operators
from flask import Flask, Blueprint
from flask_restplus import Api
from dotenv import load_dotenv


load_dotenv()

app = Flask(__name__)

api = Api(app, 
        version='1.0', 
        title='Aplication Integration WebService',
    description='Webservice which provides the resources to data integrate with AON! ontologies',
)

# Configurations
app.config.from_object('config')
blueprint = Blueprint('api', __name__, url_prefix='/api')
api.init_app(blueprint)

#from app.ms_devops.controllers import microsoft_devops_namespace
from app.jira.controllers import jira_namespace
from app.domain_name.controllers import domain_names_namespace

#api.add_namespace(microsoft_devops_namespace)
api.add_namespace(jira_namespace)
api.add_namespace(domain_names_namespace)

app.register_blueprint(blueprint)
