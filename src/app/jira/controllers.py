from flask_restplus import Resource
from rabbitmqX.patterns.client.work_queue_task_client import Work_Queue_Task_Client
from rabbitmqX.journal.journal import Journal
from app import api
from flask import request
from pprint import pprint
import json
import os
from sro_db.application import factories as application_factory
from sro_db.model import factories as model_factory


class Manager():

    def get_urls(self, organization_uuid, host):
        
        organization_application  = application_factory.OrganizationFactory()
        organization = organization_application.get_by_uuid(organization_uuid)

        applicationService = application_factory.ApplicationFactory()
        application = applicationService.retrive_by_name("jira")

        configuration_application = application_factory.ConfigurationFactory()
        configuration = configuration_application.retrive_by_organization_and_application(organization, application)[0]
        
        return {
            'online': f'http://{host}/api/jira/online/sro/{organization.uuid}/{configuration.uuid}',
            'initial_fill': f'http://{host}/api/jira/initial_fill/sro/{organization.uuid}/{configuration.uuid}'
        }


    def create_and_send_initial_fill_message(self, content, domain_name, organization_uuid, configuration_uuid=None):
        try:
            route = "integration.jira"

            data = {"domain":domain_name, 
                    "organization_uuid": organization_uuid, 
                    "configuration_uuid": configuration_uuid,
                    "content": ""}
            
            type_data = "ETL_"+domain_name.upper()

            journal = Journal(organization_uuid, type_data, data, None)

            work_queue_task_client = Work_Queue_Task_Client(route, host=os.environ.get('CLOUDAMQP_URL', 'localhost'))
            work_queue_task_client.send(journal)

        except Exception as identifier:
            pprint (identifier)
            return identifier

    def create_and_send_message(self, content, domain_name, organization_uuid, configuration_uuid=None):
        try:
            
            route = "integration.jira"
            
            content_data = ""

            if content:
                whe_ = content['webhookEvent'].split('_')
                entity = whe_[0]
                event_type = whe_[-1]
                
                entity_name = entity.split(":")[-1]
                
                pprint ("Entity Name: " +entity_name)
                
                if event_type == 'updated':
                    content_data = self._updated_event(content, entity_name)
                elif event_type == 'created': 
                    content_data = self._created_event(content, entity_name)
                elif event_type == 'deleted': 
                    content_data = self._deleted_event(content, entity_name)

            data = {"domain":domain_name, 
                    "organization_uuid": organization_uuid, 
                    "configuration_uuid": configuration_uuid,
                    "content": content_data}
            
            type_data = "Real_Time_"+domain_name.upper()

            journal = Journal(organization_uuid, type_data, data, None)
            
            work_queue_task_client = Work_Queue_Task_Client(route, host=os.environ.get('CLOUDAMQP_URL', 'localhost'))
            work_queue_task_client.send(journal)
                       
        except Exception as identifier:
            pprint (identifier)
            return identifier

    def _created_event(self, content, entity_name):
        try:
            pprint ("Create Event")
            id = content[entity_name]['id']
            if(entity_name == "issue"): 
                entity_name = content['issue']['fields']['issuetype']['name']

            return {
                        "id": id, 
                        "event_type": 'created',
                        "type": entity_name,
                        "all" : content 
                        }
                
        except Exception as identifier:
            pprint (identifier)
            return identifier
            
    def _deleted_event(self, content, entity_name):
        try:
            pprint ("Delete Event")
            id = content[entity_name]['id']
            if(entity_name == "issue"): 
                entity_name = content['issue']['fields']['issuetype']['name']

            return {
                        "id": id, 
                        "event_type": 'deleted',
                        "type": entity_name,
                        "all" : content 
                        }
                
        except Exception as identifier:
            pprint (identifier)
            return identifier

    def _updated_event(self, content, entity_name):
        try:
            pprint ("Updated Event")

            if (entity_name == "user"):
                id = content[entity_name]['accountId']
                return {
                        "id": id, 
                        "event_type": 'updated',
                        "type": entity_name, 
                        "all" : content                        
                        }
                

            id = content[entity_name]['id']
            # revision = content['changelog']['items']
            if(entity_name == "issue"): 
                entity_name = content['issue']['fields']['issuetype']['name']

            return {
                        "id": id, 
                        "event_type": 'updated',
                        "type": entity_name, 
                        "all" : content,
                        # "revision": revision
                        }
                
        except Exception as identifier:
            pprint (identifier)
            return identifier


jira_namespace = api.namespace('jira', description='Sync between Jira and some domain from seon')

@jira_namespace.route('/online/<domain_name>/<organization_uuid>/<configuration_uuid>')
@jira_namespace.param('domain_name', 'Domain name (e.g., sro and spmo)')
@jira_namespace.param('uuid', 'Organization UUID')
class ApplicationConfiguration(Resource):     

    def post(self, domain_name, organization_uuid, configuration_uuid):

        try:
            content = request.get_json(force=True)
            manager = Manager()
            manager.create_and_send_message(content, domain_name, organization_uuid, configuration_uuid)
            return "ok", 200

        except Exception as identifier:
            return identifier


@jira_namespace.route('/initial_fill/<domain_name>/<organization_uuid>/<configuration_uuid>')
@jira_namespace.param('domain_name', 'Domain name (e.g., sro and spmo)')
@jira_namespace.param('uuid', 'Organization UUID')
class ApplicationConfiguration(Resource):

    def post(self, domain_name, organization_uuid, configuration_uuid):
        
        try:
            manager = Manager()
            manager.create_and_send_initial_fill_message(None, domain_name, organization_uuid, configuration_uuid)
            return "ok", 200

        except Exception as identifier:
            return identifier


@jira_namespace.route('/config/<organization_uuid>')
@jira_namespace.param('domain_name', 'Domain name (e.g., sro and spmo)')
@jira_namespace.param('uuid', 'Organization UUID')
class ApplicationConfiguration(Resource):

    def post(self, organization_uuid):
        
        try:
            
            manager = Manager()
            urls = manager.get_urls(organization_uuid,request.host)
            return urls, 200

        except Exception as identifier:
            return identifier