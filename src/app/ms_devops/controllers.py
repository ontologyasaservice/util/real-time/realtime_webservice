from flask_restplus import Resource
from rabbitmqX.patterns.client.work_queue_task_client import Work_Queue_Task_Client
from rabbitmqX.journal.journal import Journal
from app import api
from flask import request
from pprint import pprint

class Manager():

    def create_and_send_message(self,content, domain_name, organization_uuid, configuration_uuid=None):
        try:
            route = "integration.microsoftdevops"
            
            event_type = content['eventType']
            
            if event_type == 'workitem.updated':
                content_data = self._updated_event(content)
            elif event_type == 'workitem.created': 
                content_data = self._created_event(content)
            elif event_type == 'workitem.deleted': 
                content_data = self._deleted_event(content)
            
            data = None
            
            if configuration_uuid is not None:

                data = {"domain":domain_name, 
                        "organization_uuid": organization_uuid, 
                        "configuration_uuid": configuration_uuid,
                        "content": content_data}
            else:
                data = {"domain":domain_name, 
                        "organization_uuid": organization_uuid, 
                        "content": content_data}
            
            type_data = "Real_Time_"+domain_name.upper()
            
            journal = Journal(organization_uuid, type_data, data, None)
            
            work_queue_task_client = Work_Queue_Task_Client(route)
            work_queue_task_client.send(journal)
            
        except Exception as identifier:
            return identifier

    def _created_event(self, content):
        try:
            id = content['resource']['id']
            workitem_type = content['resource']['fields']['System.WorkItemType']
            event_type = content['eventType']
                
            return {
                        "id": id, 
                        "event_type": event_type,
                        "type": workitem_type,
                        "all" : content 
                        }
                
        except Exception as identifier:
                return identifier
            
    def _deleted_event(self, content):
        try:
            id = content['resource']['id']
            event_type = content['eventType']
                
            return {
                        "id": id, 
                        "event_type": event_type,
                        "all" : content 
                        }
                
        except Exception as identifier:
            return identifier

    def _updated_event(self, content):
        try:
            id = content['resource']['id']
            workitem_type = content['revision']['fields']['System.WorkItemType']
            event_type = content['eventType']
            revision = content['resource']
                
            return {
                        "id": id, 
                        "event_type": event_type,
                        "type": workitem_type, 
                        "all" : content
                        }
                
        except Exception as identifier:
            return identifier


microsoft_devops_namespace = api.namespace('microsoft_devops', description='Sync between Microsoft DevOps and some domain from seon')

@microsoft_devops_namespace.route('/<domain_name>/<organization_uuid>')
@microsoft_devops_namespace.param('domain_name', 'Domain name (e.g., sspo and spmo)')
@microsoft_devops_namespace.param('uuid', 'Organization UUID')
class Application(Resource):     
           
    def post(self, domain_name, organization_uuid):
        
        try:
            content = request.get_json(force=True)
            manager = Manager()
            manager.create_and_send_message(content, domain_name, organization_uuid)
            return "ok", 200

        except Exception as identifier:
            return identifier

@microsoft_devops_namespace.route('/<domain_name>/<organization_uuid>/<configuration_uuid>')
@microsoft_devops_namespace.param('domain_name', 'Domain name (e.g., sspo and spmo)')
@microsoft_devops_namespace.param('uuid', 'Organization UUID')
class ApplicationConfiguration(Resource):     
           
    def post(self, domain_name, organization_uuid, configuration_uuid):
        
        try:
            content = request.get_json(force=True)
            manager = Manager()
            manager.create_and_send_message(content, domain_name, organization_uuid, configuration_uuid)
            return "ok", 200

        except Exception as identifier:
            return identifier

